<?php

namespace Drupal\taxonomy_translator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy_translator\BatchTermTranslator;

/**
 * Class BatchTermTranslatorForm - form for running batch process of the translator
 */
class BatchTermTranslatorForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_translator_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['markup'] = [
        '#markup' => 'Options for the creation of term new language'
    ];

    $vocabularies = \Drupal::entityQuery('taxonomy_vocabulary')->execute();
    $form['vocabulary'] = [
      '#type' => 'select',
        '#title' => 'Vocabulary of the terms',
        '#options' => $vocabularies,
    ];

    $langcodes = \Drupal::languageManager()->getLanguages();
    $langList = [];
    foreach ($langcodes as $langKey=>$lang) {
      if($langKey == 'en') continue;
      $langList[$langKey] = $lang->getName();
    }
    $form['language'] = [
      '#type' => 'select',
      '#title' => 'Target language',
      '#options' => $langList,
    ];

    $form['skip_count'] = [
        '#type' => 'textfield',
        '#title' => 'Term\'s offset count',
        '#default_value' => 0,
    ];
    $form['count_limit'] = [
        '#type' => 'textfield',
        '#title' => 'Maximum of the term count to processing',
        '#default_value' => 0,
    ];
    $form['step_size'] = [
        '#type' => 'textfield',
        '#title' => 'Number of terms to load for each query',
        '#default_value' => 20,
    ];
    $form['term_id'] = [
        '#type' => 'textfield',
        '#title' => 'Term id',
        '#description' => 'To translate only the term with this id',
        '#default_value' => 0,
    ];
    $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vocabulary = $form_state->getValue('vocabulary');
    $language = $form_state->getValue('language');
    $skip_count = $form_state->getValue('skip_count');
    $count_limit = $form_state->getValue('count_limit');
    $step_size = $form_state->getValue('step_size');
    $term_id = $form_state->getValue('term_id');
    $batch = new BatchTermTranslator($vocabulary, $language, $skip_count, $step_size, $count_limit, $term_id);
    $batch->execute();
  }

}
