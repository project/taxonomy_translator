<?php

namespace Drupal\taxonomy_translator\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure settings for the module.
 */
class ModuleSettings extends ConfigFormBase {

  use StringTranslationTrait;

  const CONFIG_NAME = 'taxonomy_translator.settings';

  private $fieldList = [ 'language', 'google_credential_location', 'google_project_id', 'drupal2google_languages', ];

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [ self::CONFIG_NAME, ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return str_replace('.', '_',  self::CONFIG_NAME);
  }



  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $langcodes = \Drupal::languageManager()->getLanguages();
    $langList = [];
    foreach ($langcodes as $langKey=>$lang) {
      $langList[$langKey] = $lang->getName();
    }
    $form['language'] = [
      '#type' => 'select',
      '#title' => 'Source language',
      '#options' => $langList,
      '#description' => $this->t('Main site language used on the site'),
      '#default_value' => $config->get('language'),
    ];

    $form['google_credential_location'] = [
      '#title' => $this->t('Google Credential File Location'),
      '#type' => 'textfield',
      '#placeholder' => 'For example, ../MyDrupalProject-202937bd51e2.json',
      '#description' => $this->t('Path of the json-file with google credential information'),
      '#default_value' => $config->get('google_credential_location'),
    ];

    $form['google_project_id'] = [
      '#title' => $this->t('Google Project ID'),
      '#type' => 'textfield',
      '#placeholder' => 'For example, mydrupalproject-202901',
      '#description' => $this->t('Id of you google cloud project'),
      '#default_value' => $config->get('google_project_id'),
    ];

    $form['drupal2google_languages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Language conversion ids from drupal id to google translator id'),
      '#description' => $this->t('If language id in the google translator is not match drupal language id, you need to write here the line in the format DrupalLangId|GoogleTranslatorId, (for example: "zh-hans|zh") for each needed languages.'),
      '#default_value' => $config->get('drupal2google_languages'),
    ];

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config(self::CONFIG_NAME);
    foreach ($this->fieldList as $fldName) {
      $settings->set($fldName, $form_state->getValue($fldName));
    }

    // Save all configurations.
    $settings->save();
    parent::submitForm($form, $form_state);
  }

}
