<?php

/**
 * Main class for taxonomy_translator module
 * It create and run batch to translate taxonomy terms
 */
namespace Drupal\taxonomy_translator;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\Entity\Term;
use Google\Cloud\Translate\V2\TranslateClient;


class BatchTermTranslator {

  use StringTranslationTrait;

  /** @var string $vocabulary - vocabulary name */
  private $vocabulary;
  /** @var string $language - target language id */
  private $language = 'ru';
  /** @var string $sourceDrupalLangId - source language id */
  private $sourceDrupalLangId = 'en';
  /** @var integer $skip_count - how many terms to skip */
  private $skip_count = 0;
  /** @var integer $count_limit - how many terms to translate */
  private $count_limit = 0;
  /** @var integer $term_id - taxonomy term id to translate only that */
  private $term_id = 0;
  /** @var integer $count_limit - how many terms to translate by one step */
  private $step_size;

  /** @var \Drupal\Taxonomy\TermStorage $termStorage */
  private $termStorage;

  /** @var string $exploder - string to separate tid and taxonomy name in translation text */
  private $exploder = ' %%% ';
  /** @var string $exploder - string to separate lines in translation text */
  private $lineExploder = ' @@@ ';


  /**
   * BatchTermTranslator constructor.
   *
   * @param string $vocabulary - vocabulary id for taxonomy terms to translate
   * @param string $language - target language id
   * @param int $skip_count - how many terms to skip
   * @param int $step_size - item count for one step
   * @param int $count_limit - maximum step number for total batch
   * @param int $term_id - id of the term for translation
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct($vocabulary, $language, $skip_count, $step_size, $count_limit=0, $term_id=0) {
    $this->vocabulary = trim($vocabulary);
    $this->language = trim($language);
    $this->skip_count = (int)$skip_count;
    $this->count_limit = (int)$count_limit;
    $this->step_size = $step_size>0 ? (int)$step_size : 10;
    $this->term_id = (int)$term_id;
    $this->sourceDrupalLangId = \Drupal::config('taxonomy_translator.settings')->get('language');

    //$this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

  }


  /**
   * Getting list of needed taxonomy terms from database
   *
   * @return array
   */
  private function getTermList() {
    $termList = [];

    $database = \Drupal::database();
    $query = $database->select($this->termStorage->getDataTable(), 't');
    $query->leftJoin($this->termStorage->getDataTable(), 't2', 't2.tid = t.tid and t2.langcode=:langcode', [ ':langcode'=>$this->language ]); //without language of translation

    $count = 1000000;
    if($this->count_limit>0) $count = $this->count_limit;
    $result = $query
      ->addTag('taxonomy_term_access')
      ->fields('t', ['tid', 'name'])
      ->condition('t.vid', $this->vocabulary)
      //->condition('t.default_langcode', 1)
      ->condition('t.langcode', $this->sourceDrupalLangId)
      ->condition('t2.tid', null, 'IS NULL')
      ->orderBy('t.tid')
      ->range($this->skip_count, $count)
      ->execute();

    foreach ($result as $term) {
      $termList[$term->tid] = $term->name;
    }
    return $termList;
  }


  /**
   * Creating and running batch
   */
  public function execute() {
    $options = [];
    $options[] = [[$this, 'prepareBeforeExecute'], []];

    if ($this->term_id) {
      $options[] = [[$this, 'translation'], ['content' => [$this->term_id]]];
    } else {
      $termData = $this->getTermList();
      $totalCnt = count($termData);
      for ($i=0; $i<$totalCnt; $i+=$this->step_size) {
        $stepTermData = array_slice($termData, $i, $this->step_size, true);
        $options[] = [[$this, 'translation'], ['content' => $stepTermData]];
      }
    }

    $batch = [
      'title' => t('Processing term translation'),
      'operations' => $options,
      'finished' => '\Drupal\taxonomy_translator\BatchTermTranslation::finishedCallback',
      'init_message' => t('Start term translation'),
      'error_message' => t('Term translator has encountered an error.'),
      'progress_message' => t('Processed @current out of @total. Estimated time: @estimate.'),
    ];
    batch_set($batch);
  }


  /**
   * First step of the batch
   *
   * @param $context
   */
  public function prepareBeforeExecute(&$context) {
    $context['results']['total'] = 0;
    $context['results']['skipped'] = 0;
    $context['results']['processed'] = 0;
  }


  /**
   * Batch step
   *
   * @param array $termData - list of terms for translation
   * @param array $context - context variables
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function translation($termData, &$context) {
    set_time_limit(600);

    if(!count($termData)) return false;

    $context['results']['total'] += count($termData);

    $skipped = [];
    $used = [];
    $titles = '';
    /** @var Term $term */
    foreach ($termData as $tid=>$termName) {
//      $term = $this->termStorage->load($tid);
//      if(!$term->hasTranslation($this->sourceDrupalLangId)
//            or $term->hasTranslation($this->language)) {
//        $skipped[] = $term->id();
//        continue;
//      }
      $titles .= $this->lineExploder.$tid.$this->exploder.$termName;
      $used++;
    }

    if(count($skipped)) {
      \Drupal::logger('Translation step skipped terms: '.implode(', ', $skipped))->notice('notice');
      $context['results']['skipped'] += count($skipped);
    }

    $tr = $this->getTranslator();
    $translatedTitlesTxt = $this->removeGoogleGarbage($titles ? $tr->translate($titles)['text'].'' : '');

    $translatedTitles = [];
    foreach (preg_split('/'.trim($this->lineExploder).'/', $translatedTitlesTxt) as $line) {
      $exploder = trim($this->exploder);
      $lineArr = array_map('trim', preg_split('/'.$exploder.'/', $line));
      if(!(count($lineArr)>1)) continue;
      $tid = (int) trim(array_shift($lineArr));
      $translatedTitles[$tid] = implode($exploder, $lineArr);
    }

    foreach ($termData as $tid=>$termName) {
      if(!isset($translatedTitles[$tid])) continue;

      $term = $this->termStorage->load($tid);

      $term->addTranslation($this->language, ['name' => $translatedTitles[$tid]]);
      $term->save();
      $context['results']['processed']++;
    }
    return true;
  }


  /**
   * Get google translation object to translate text
   *
   * @return TranslateClient
   */
  private function getTranslator() {
    $langConv = $this->getLanguageIdConversionConf(); //['zh-hans'=>'zh'];
    $soruceLangId = $langConv[$this->sourceDrupalLangId] ?? $this->sourceDrupalLangId;
    $translLangId = $langConv[$this->language] ?? $this->language;

    $path2GKJson = \Drupal::config('taxonomy_translator.settings')->get('google_credential_location');
    $projectId = \Drupal::config('taxonomy_translator.settings')->get('google_project_id');

    putenv('GOOGLE_APPLICATION_CREDENTIALS='.DRUPAL_ROOT.'/'.$path2GKJson);
    return new TranslateClient([ 'projectId' => $projectId,
      'source'=>$soruceLangId, 'target'=>$translLangId, ]);
  }


  private function getLanguageIdConversionConf() {
    $langConvertConf = \Drupal::config('taxonomy_translator.settings')->get('drupal2google_languages');
    $langConv = [];
    foreach (explode("\n", $langConvertConf) as $langConvLine) {
      if(!$langConvLine) continue;
      $langArr = array_map('trim', explode('|', $langConvLine));
      if(count($langArr)>1) $langConv[$langArr[0]] = $langArr[1];
    }
    return $langConv;
  }


  private function removeGoogleGarbage($str) {
    $str = str_replace('&quot;', "\"", $str);
    $str = preg_replace([ '/(@@)\s+(@)/u', '/(@)\s+(@@)/u', '/(%%)\s+(%)/u', '/(%)\s+(%%)/u', ], '${1}${2}', $str);
    $str = preg_replace('/(@{4,})/u', '@@@', $str);
    $str = preg_replace('/(%{4,})/u', '%%%', $str);
    return $str;
  }



  public static function finishedCallback($success, $results, $operations) {
    if ($success) {
      $context['results']['total'] = 0;
      $context['results']['skipped'] = 0;
      $context['results']['processed'] = 0;

      $message = \Drupal::translation()->formatPlural(
        $results['counts']['total'],
        'Processing result: total - :c_total, processed -  :c_processed, skipped -  :c_skipped', 'total - :c_total, processed -  :c_processed, skipped -  :c_skipped',
        [
          ':c_total' =>  $results['counts']['total'],
          ':c_processed' =>  $results['counts']['skipped'],
          ':c_skipped' =>  $results['counts']['processed'],
        ]
      );
    } else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }


}
